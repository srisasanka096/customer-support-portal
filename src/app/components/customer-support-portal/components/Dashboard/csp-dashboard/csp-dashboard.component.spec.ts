import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CSPDashboardComponent } from './csp-dashboard.component';

describe('CSPDashboardComponent', () => {
  let component: CSPDashboardComponent;
  let fixture: ComponentFixture<CSPDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSPDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSPDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
