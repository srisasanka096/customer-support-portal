import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-master',
  templateUrl: './product-master.component.html',
  styleUrls: ['./product-master.component.scss']
})
export class ProductMasterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  saveProductMaster() {
    console.log('Entered Data');
  }
  resetProductMaster() {
    console.log('Reset Data');
  }
}
