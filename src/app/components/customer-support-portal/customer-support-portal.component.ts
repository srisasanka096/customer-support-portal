import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-customer-support-portal',
  templateUrl: './customer-support-portal.component.html',
  styleUrls: ['./customer-support-portal.component.scss']
})
export class CustomerSupportPortalComponent implements OnInit {

  constructor( private route: ActivatedRoute,
               private router: Router,
               private authService: AuthService, ) { }

  ngOnInit(): void {
  }
  logOut() {
    console.log('Log Out Successfully.!');
    this.authService.logout();
   }

   goToDashBoard() {
    this.router.navigate(['CustomerSupportPortal/administration/dashboard']);
   }
}
