import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerSupportPortalComponent } from './customer-support-portal.component';

describe('CustomerSupportPortalComponent', () => {
  let component: CustomerSupportPortalComponent;
  let fixture: ComponentFixture<CustomerSupportPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerSupportPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerSupportPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
