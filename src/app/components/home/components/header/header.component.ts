import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpService } from 'src/app/services/http/http.service';
import { Router } from '@angular/router';
import { LoginComponent } from 'src/app/components/auth/components/login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  name: any;
  email: any;

  constructor( public dialog: MatDialog,
               private httpService: HttpService,
               private router: Router, ) { }

  ngOnInit(): void {
  }
  openDialog(): void {
    /* const dialogRef = this.dialog.open(LoginComponent, {
      width: '500px',
      data: {name: this.name, email: this.email}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.email = result;
    }); */
    this.login();
  }

  login() {
    // this.router.navigate(['/CustomerSupportPortal']);
    this.router.navigateByUrl('/CustomerSupportPortal').then(() => {});
  }
}
