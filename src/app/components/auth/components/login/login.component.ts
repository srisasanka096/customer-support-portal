import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// import { DialogData } from 'src/app/components/home/components/header/header.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  data: any;

  constructor(
              //   public dialogRef: MatDialogRef<LoginComponent>,
              //  @Inject(MAT_DIALOG_DATA) public data: DialogData,
               private route: ActivatedRoute,
               private router: Router, ) { }

  ngOnInit(): void {
    this.data.name = 'CSP!';
  }
  /* cancelLogin(): void {
    this.dialogRef.close();
  } */

  login() {
    // this.router.navigate(['/CustomerSupportPortal']);
    this.router.navigateByUrl('/CustomerSupportPortal').then(() => {});
  }
}
