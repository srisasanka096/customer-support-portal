import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  roleScreens: Array<any> = [];
  rolesData: Array<any> = [];
  screenDetails: any = {};
  constructor( private httpService: HttpService,
               private authService: AuthService, ) { }

    getRoleScreensByUser() {
    const obj: any = {
      loginId: this.authService.loginId,
      accessLevelId: this.authService.accessLevelId,
    };
    this.httpService.postData('/api/getRoleScreensByUser', obj).subscribe(res => {
      if (res) {
        const data: any = res;
        this.roleScreens = data;
      } else {
        this.roleScreens = [];
      }
    }, error => {
      this.roleScreens = [];
      /*this.roleScreens = [
        {
          screenName: 'schoolDetails',
          screenCode: 1,
          canAmend: 1,
          canDelete: 0,
          canCreate: 0,
          canApprove: 1,
        }
      ];*/
    });
  }

  getScreenDetailsById(id) {
    const obj: any = {};
    obj.id = id;
    this.httpService.postData('getScreenDetailsById', obj).subscribe(res => {
      if (res) {
      const data: any = res;
      this.screenDetails = data;
      } else {
        this.screenDetails = {};
      }
    });
  }
}
