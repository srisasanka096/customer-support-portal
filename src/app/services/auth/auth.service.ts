import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string;
  loginId: string;
  accessLevelId: string;
  userDetailsByLoginId: any = {};
  constructor( private httpService: HttpService,
               private router: Router, ) { }
               getAuthToken() {
                return this.token;
              }
    getUserDetailsByLoginId(loginId: string) {
      const obj: any = {};
      obj.loginId = this.loginId;
      this.httpService.postData('getUserDetailsByLoginId', obj).subscribe(res => {
        if (res) {
          const data: any = res;
          this.userDetailsByLoginId = data;
        }
      });
    }
    logout() {
      this.token = undefined;
      this.accessLevelId = undefined;
      this.loginId = undefined;
      this.router.navigateByUrl('/');
    }
    login(credentials: {userName, password}) {
      const loginCredentials = {...credentials};
      return this.httpService.postData('/api/scholllogin', loginCredentials);
    }
}
