import { TestBed } from '@angular/core/testing';

import { CspResolverService } from './csp-resolver.service';

describe('CspResolverService', () => {
  let service: CspResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CspResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
