import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app/app.service';

@Injectable({
  providedIn: 'root'
})
export class CspResolverService implements Resolve<any> {

  constructor( private cspService: AppService, ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.cspService.roleScreens;
  }
}
