import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CspRoutingModule } from './csp-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerSupportPortalComponent } from 'src/app/components/customer-support-portal/customer-support-portal.component';
import { SidebarComponent } from 'src/app/components/customer-support-portal/components/Dashboard/Navigation/sidebar/sidebar.component';


@NgModule({
  declarations: [
    CustomerSupportPortalComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    CspRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CspModule { }
