import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerSupportPortalComponent } from 'src/app/components/customer-support-portal/customer-support-portal.component';


const routes: Routes = [
  {
    path: '',
    component: CustomerSupportPortalComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'administration'
      },  {
        path: 'administration',
        loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule),
      }, /*{
        path: 'academics',
        loadChildren: () => import('../academics/academics.module').then(m => m.AcademicsModule),
      } */
    ],
    // canActivateChild: [RoleGuard],
  },
  /* {
    path : 'profile',
    component : SchoolProfileComponent
  } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CspRoutingModule { }
