import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/components/home/home.component';
import { HomePageComponent } from 'src/app/components/home/components/home-page/home-page.component';
import { ContactComponent } from 'src/app/components/home/components/contact/contact.component';
import { AboutComponent } from 'src/app/components/home/components/about/about.component';
import { ServicesComponent } from 'src/app/components/home/components/services/services.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'home-page',
        component: HomePageComponent
      }, {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'services',
        component: ServicesComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home-page'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
