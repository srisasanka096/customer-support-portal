import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from 'src/app/components/home/home.component';
import { HomePageComponent } from 'src/app/components/home/components/home-page/home-page.component';
import { HeaderComponent } from 'src/app/components/home/components/header/header.component';
import { FooterComponent } from 'src/app/components/home/components/footer/footer.component';
import { ContactComponent } from 'src/app/components/home/components/contact/contact.component';
import { AboutComponent } from 'src/app/components/home/components/about/about.component';
import { ServicesComponent } from 'src/app/components/home/components/services/services.component';
import { LoginComponent } from 'src/app/components/auth/components/login/login.component';


@NgModule({
  declarations: [
    HomeComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    ContactComponent,
    AboutComponent,
    ServicesComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    LoginComponent
  ],
})
export class HomeModule { }
