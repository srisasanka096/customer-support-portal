import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CSPDashboardComponent } from 'src/app/components/customer-support-portal/components/Dashboard/csp-dashboard/csp-dashboard.component';
import { AdminComponent } from 'src/app/components/customer-support-portal/components/admin/admin.component';
import { ProductMasterComponent } from 'src/app/components/customer-support-portal/components/admin/components/product-master/product-master.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard',
      }, {
        path: 'dashboard',
        component: CSPDashboardComponent,
      },
       {
        path: 'product-master',
        component: ProductMasterComponent,
      },
      /*{
        path: 'admission-enquiry',
        component: AdmissionEnquiryComponent,
      },
      {
        path: 'admission-enquiry-list',
        component: AdmissionEnquiryListComponent,
      },
      {
        path: 'expense-details',
        component: ExpenseDetailsComponent,
      },
      {
        path: 'application-list',
        component: ListApplicationComponent,
      },
      {
        path: 'record-recipt',
        component: RecordReceiptsComponent,
      },
      {
        path: 'standards-in-schools',
        component: StandardsInSchoolComponent,
      },
      {
        path: 'source-details',
        component: SourceDetailsComponent,
      },
      {
        path: 'fee-structures',
        component: FeeStructuresComponent,
      },
      {
        path: 'joining-number',
        component: JoiningNumberComponent,
      },
      {
        path: 'fee-recipt',
        component: FeeReceiptComponent,
      }, */
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
