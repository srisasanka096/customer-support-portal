import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from 'src/app/components/customer-support-portal/components/admin/admin.component';
import { CSPDashboardComponent } from 'src/app/components/customer-support-portal/components/Dashboard/csp-dashboard/csp-dashboard.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductMasterComponent } from '../../components/customer-support-portal/components/admin/components/product-master/product-master.component';


@NgModule({
  declarations: [
    AdminComponent,
    CSPDashboardComponent,
    ProductMasterComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AdminModule { }
