import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnderConstructionComponent } from './components/helpers/under-construction/under-construction.component';
import { CspResolverService } from './helpers/resolvers/CSP/csp-resolver.service';
import { PageNotFoundComponent } from './components/helpers/page-not-found/page-not-found.component';
import { UnauthorizedComponent } from './components/helpers/unauthorized/unauthorized.component';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
  }, {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
  }, {
    path: 'CustomerSupportPortal',
    loadChildren: () => import('./modules/csp/csp.module').then(m => m.CspModule),
   /*  canActivate: [AuthGuard], */
    resolve: {
      roleScreens: CspResolverService
    }
  }, {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  }, {
    path: 'under-construction',
    component: UnderConstructionComponent,
  }, {
    path: 'unauthorized',
    component: UnauthorizedComponent,
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
